<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'C:\xampp\htdocs\dsb\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'wp_dsb');

/** MySQL database username */
define('DB_USER', 'dsb_user');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Define if the theme is in developmet or production mode **/
define('WP_ENV', 'development');

define ('WPLANG', 'ro_RO');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bk#X=Y 9.&BPQ@WlvfafN5Ci-g*muvxU4n,(D+[N_lC-bS/yjLcB+-9g+R_dH7j(');
define('SECURE_AUTH_KEY',  '/(Q^4-igS*S>nXdF@#pwCHdzSQGptO9sR_8S-FM59CpmMYrUxUf|,E&8,~C_84l_');
define('LOGGED_IN_KEY',    ',3k&Q|]@=2d(,A:w6XQn2,6y[->I)y~[.oiL5$Te$18`l+Gf,9CvZOL6^zPhsQ-6');
define('NONCE_KEY',        'q^I1>QjX|!*9pTKiRVseQb7#|s>,nX&wxt(F6;!frUXsFl|+_6]D^Wp7|8rQdHJ:');
define('AUTH_SALT',        'E-<|X<A-|*`f:EBFFz>a+O8kWP$}I7&*X|dLax>Fur}|}# +Z18$@eDD%/RA;@E0');
define('SECURE_AUTH_SALT', '7sC?]# kz+2x>`D,NSzJEgo%5g-ZA{Jqk)R$ lc1,6dHivur9cp+|eshiiR@W[+L');
define('LOGGED_IN_SALT',   'N[APw/tSV-+H-Tr*-pUFVi0Zd=j4sgXQRIemU@N>kS4-tr!ViN[wsH{!8G~o&8_|');
define('NONCE_SALT',       'ic|~.-z&wXJeFlH%54]~HSS)jahPsjoJiIVdF1+#yKXbZ7-/~xVn%36x99L-6?`2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
